# RPG Character System

## Introduction

RPG Character System is a console application written in Java which lets the user create characters, and manipulate their stats.
A character is either a Mage, Ranger or a Warrior. The characters start off at level 1 with base stats according to their class. 
When a character is created the user can give it experience and if given enough the character will increase its level accordingly. 

The user can also create various items. These include weapons and armor. Weapons can be either Melee, Ranged or Magic. These also starts off at level 1 with base damage they deal. The user can set their item level and the damage they do is increased as the level is increased. Armor items consists of either Cloth, Leather or Plate armor. Same with weapon these start off at level 1 and the user can set their level. Armorpieces has base stats including Intelligence, Strength, Dexterity and HP. The armoritems are also divided in what slot they are, either Body-piece, Head-piece or Leg-piece. The basestats they have are scaled depening on what slot type they have. 

When the user has created a Character and Items, it can make the Character equip these items. When an item is equipped the stats of the character is increased based on the stats of the set item. When a character has a Weapon equipped the character can attack. The damage the weapon deals is based on the stats of the character and the type of weapon. For example, the damage dealt with a magic weapon is scaled with Intelligence. So the more Intelligence a character has from either level or equipped armor, the more damage the weapon deal. 

From the menu the user is able to browse throug the items and characters it has created. 

NB. The program is hardcoded with different items and characters already included. 


## Structure

The application consist of these classes.
* Champion (Abstract Class)
    * Mage
    * Warrior
    * Ranger
* Item (Interface) 
    * Armor (Abstract Class)
        * ClothArmor
        * LeatherArmor
        * PlateArmor
    * Weapon
        * MagicWeapon
        * RangedWeapon
        * MeleeWeapon
* Main (main program)



#### UML'S

UML for Champion and its sub-classes

![championUML](images/championUML.png)



UML for Item interface, Armor and its sub-classes

![armorUML](images/armorUML.png)


UML for Item interface, Weapon and its sub-classes

![weaponUML](images/weaponUML.png)


## Instruction

To use the application you need to have Java SDK installed. 
To run from terminal: 
java -jar pathToJarfile(located inside out/artifacts). 


## Visuals. 

Various printscreens from the application. 

##### Create Character.
Presentation of creating a character from main menu. 

![createCharacter](images/createCharacter.PNG)

##### Browse characters.
Presentation of browsing throug characters from main menu. 
The user can type the name of the character and gets redirected to the Character screen displaying it's stats and lets it manipulate the character. 

![browseChar](images/browseChar.PNG)

##### Equip gear to character.
Presentation of the process of equipping gear to the character. 

![addGear](images/addGear.PNG) 

##### Creating items.
Presentation of the process of creating a weapon and giving it the desired lvl. An item can only have its level set once. 

![createWeapon](images/createWeapon.PNG)


##### Browse items.
From the main menu the user can browse throug items created. 

![browseItems](images/browseItems.PNG)



