package com.company.Heroes;

import com.company.items.Armor;
import com.company.items.Weapon;

import java.util.HashMap;
import java.util.Map;

public abstract class Champion {

    protected int hp;
    protected int strength;
    protected int dexterity;
    protected int intelligence;
    protected int level;
    protected int experience;
    protected String heroType;
    protected String name;

    protected Map<String,Armor> equipment = new HashMap<>();
    protected Map<String, Weapon> weaponEquipment = new HashMap<>();


    public Champion(int hp, int strength, int dexterity, int intelligence, int level, int experience, String heroType) {
        this.hp = hp;
        this.strength = strength;
        this.dexterity = dexterity;
        this.intelligence = intelligence;
        this.level = level;
        this.experience = experience;
        this.heroType = heroType;

    }

    public Map<String, Weapon> getWeaponEquipment() {
        return weaponEquipment;
    }

    public void setWeaponEquipment(Map<String, Weapon> weaponEquipment) {
        this.weaponEquipment = weaponEquipment;
    }

    public Map<String, Armor> getEquipment() {
        return equipment;
    }

    public void setEquipment(Map<String, Armor> equipment) {
        this.equipment = equipment;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getStrength() {
        return strength;
    }


    public int getDexterity() {
        return dexterity;
    }

    public int getIntelligence() {
        return intelligence;
    }


    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public int getExperience() {
        return experience;
    }

    public void setExperience(int experience) {
        this.experience = experience;
    }

    public String getHeroType() {
        return heroType;
    }

    public void setHeroType(String heroType) {
        this.heroType = heroType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void gainExperience(int experience) {

        if(experience >= experienceNeededToLevelUp()) {
            levelUp();
            setExperience(experience - experienceNeededToLevelUp());
            level += 1;
        }else {
            this.experience = this.experience + experience;
        }
    }

    public void gainExperience2(int exp) {

            while(exp >= experienceNeededToLevelUp()) {
                exp = exp - experienceNeededToLevelUp();
                levelUp();
                level +=1;
            }
            experience = exp;
        }




    //calculate experience needed to level up per level.
    public int experienceNeededPerLevel(int level) {
        int result = 100;
        for(int i = 2; i <= level; i++){
            result += (result / 10);
            //   System.out.println(result);
        }
        return result;
    }

    //calculate remaining experience to level up.
    public int experienceNeededToLevelUp(){
        return experienceNeededPerLevel(getLevel()) - getExperience();

    }


    abstract public void levelUp();


}
