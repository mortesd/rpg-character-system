package com.company.Heroes;

public class Mage extends Champion{


    public Mage(){
        super(100,2,3,10,1,0,"Mage");
    }

    @Override
    public void levelUp() {
        hp += 15;
        strength += 1;
        dexterity += 2;
        intelligence += 5;
    }

}
