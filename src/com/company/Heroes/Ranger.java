package com.company.Heroes;

public class Ranger extends Champion {


    public Ranger(){
        super(120,5,10,2,1,0,"Ranger");
    }

    @Override
    public void levelUp() {
        hp += 20;
        strength += 2;
        dexterity += 5;
        intelligence += 1;
    }
}
