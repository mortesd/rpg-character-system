package com.company.Heroes;

public class Warrior extends Champion{



    public Warrior(){
        super(150,10,3,1,1,0,"Warrior");
    }


    @Override
    public void levelUp() {
        hp += 30;
        strength += 5;
        dexterity += 2;
        intelligence += 1;
    }

}
