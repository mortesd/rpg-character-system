package com.company;

import com.company.Heroes.Champion;
import com.company.Heroes.Mage;
import com.company.Heroes.Ranger;
import com.company.Heroes.Warrior;
import com.company.items.Armor;
import com.company.items.Weapon;
import com.company.items.armor.ClothArmor;
import com.company.items.armor.LeatherArmor;
import com.company.items.armor.PlateArmor;
import com.company.items.weapon.MagicWeapon;
import com.company.items.weapon.MeleeWeapon;
import com.company.items.weapon.RangedWeapon;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.*;

public class Main {

    Collection<Champion> champions = new ArrayList<>();
    Collection<Weapon> weapons = new ArrayList<>();
    Collection<Armor> armorPieces = new ArrayList<>();


    public Main() {

    }

    // The main menu of the program.
    public void mainMenu() throws InterruptedException {
        Scanner myScanner = new Scanner(System.in);
        System.out.println("------------------------------------");
        System.out.println("Main Menu");
        System.out.println("------------------------------------");
        System.out.println("1. Create new character");
        System.out.println("2. Browse characters");
        System.out.println("3. Create items");
        System.out.println("4. Browse items");
        System.out.println("5. Exit program");
        int choice = myScanner.nextInt();
        try{
            if(choice == 1){
                createCharacter();
            }else if(choice == 2){
                browseCharacters();
            }else if(choice == 3) {
                createItem();
            }else if(choice == 4) {
                browseItem();
            }else if(choice == 5) {
                System.exit(0);
            }else{
                throw new IllegalArgumentException("illegal input");
            }

        } catch (Exception ex)  {
            System.out.println(ex);
            System.out.println("You will be redirected to the main menu in 2 seconds");
            Thread.sleep(2000);
            mainMenu();
        }
        mainMenu();
    }


    //Lets you create Items, both weapon and armor.
    public void createItem() throws InterruptedException {
        Scanner myScanner = new Scanner(System.in);
        System.out.println("------------------------------------");
        System.out.println("Create new Item");
        System.out.println("------------------------------------");
        System.out.println("1. Weapon");
        System.out.println("2. Armor");
        System.out.println("3. Back to menu");
        try {
            int choice = myScanner.nextInt();
            if (choice == 1) {
                createWeapon();
            } else if (choice == 2) {
                createArmor();
            } else if (choice == 3) {
                mainMenu();
            }
        } catch (Exception ex) {
            System.out.println(ex);
            System.out.println("You will be redirected to the create item menu in 2 seconds");
            Thread.sleep(2000);
            createItem();
        }
    }
    // Method to create Armor.
    public void createArmor() throws InterruptedException {
        Scanner myScanner = new Scanner(System.in);
        String armorType = "";
        System.out.println("------------------------------------");
        System.out.println("Create new Armor");
        System.out.println("------------------------------------");
        System.out.println("1. Cloth");
        System.out.println("2. Leather");
        System.out.println("3. Plate");
        System.out.println("4. Choose a different item type");

        int choice = myScanner.nextInt();

        //Calls slotMenu() with parameter of armortype. So when it is called you create armor piece with correct armortype.
        if(choice == 1) {
            armorType = "cloth";
            slotMenu(armorType);
        }else if(choice == 2) {
            armorType = "leather";
            slotMenu(armorType);
        }else if(choice == 3) {
            armorType ="plate";
            slotMenu(armorType);
        }else if(choice == 4) {
            createItem();
        }
    }

    //Takes armortype to create a piece of equipment with the correct armortype to the chosen slot-type.
    public void slotMenu(String armortype) throws InterruptedException {
        String slotType = "";
        Scanner myScanner = new Scanner(System.in);
        System.out.println("------------------------------------");
        System.out.println("Choose slot for your " + armortype +" piece.");
        System.out.println("------------------------------------");
        System.out.println("1. Head");
        System.out.println("2. Body");
        System.out.println("3. Legs");
        System.out.println("4. Choose different armortype");

        int choice = myScanner.nextInt();

        // Depening on choice slotype String varies from the different choices and gets passed along with the call to newArmorPiece alongside armortype
        //chosen in the CreateArmor() method.
        //Catches IO exception because newArmor piece uses buffered reader.
        if(choice == 1) {
            slotType = "head";
            try {
                newArmorPiece(slotType, armortype);
            }catch (IOException ex) {
                System.out.println(ex);
            }

        }else if(choice == 2) {
            slotType = "body";
            try {
                newArmorPiece(slotType, armortype);
            }catch (IOException ex) {
                System.out.println(ex);
            }
        }else if(choice == 3) {
            slotType = "legs";
            try {
                newArmorPiece(slotType, armortype);
            }catch (IOException ex) {
                System.out.println(ex);
            }
        }else if(choice == 4) {
            createArmor();
        }
    }

    //finally after choosing slottype and armortype you create armor with those parameters.
    public void newArmorPiece(String slotType, String armorType) throws IOException, InterruptedException {
        Scanner myScanner = new Scanner(System.in);
        System.out.println("------------------------------------");
        System.out.println("Create new " + armorType + " " + slotType);
        System.out.println("------------------------------------");
        System.out.println("1. Set name of armorpiece");
        System.out.println("2. Choose another slotType");

        int choice = myScanner.nextInt();

        if(choice == 1) {
            Armor armor = null;
            //creates armorpiece depending on type. For instance if you chose cloth in the createArmor() method you now create a ClothArmor().
            if(armorType.equals("cloth")) {
                armor = new ClothArmor();
            }else if(armorType.equals("leather")){
                armor = new LeatherArmor();
            }else if(armorType.equals("plate")){
                armor = new PlateArmor();
            }
            String name ="";
            System.out.println("Enter name");

            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            name = reader.readLine();

            //sets the name and slottype of the newly created piece.
            armor.setName(name);
            armor.setSlotType(slotType);
            //adds the piece to a Collection of pieces.
            armorPieces.add(armor);
            //calls armorScreen() with the newly created armor.
            armorScreen(armor);


        }else if(choice == 2) {
            slotMenu(armorType);
        }

    }

    //ArmorScreen shows you the stats of the created weapon and lets you set its itemlevel ONCE.
    // After you have set the itemlevel, the method calls itself recursivly and showcases the item with new stats scaled by lvl.
    // Now the 1. Give level is no longer an option.
    public void armorScreen(Armor armor) {
        Scanner myScanner = new Scanner(System.in);
        System.out.println("------------------------------------");
        System.out.println(armor.getName());
        System.out.println("------------------------------------");
        System.out.println("Armor Type: " + armor.getArmorType());
        System.out.println("Slot: " + armor.getSlotType());
        System.out.println("Armor Level :" + armor.getLevel());
        System.out.println("Bonus HP: " + armor.getHp());
        System.out.println("Bonus dex: " + armor.getDex());
        System.out.println("Bonus Strength: " + armor.getStrength());
        System.out.println("Bonus Int: " + armor.getIntelligence());

        System.out.println("------------------------------------");
        if(armor.getLevel() == 1) {
            System.out.println("1. Give LVL");
        }

        System.out.println("2. Back to Menu");
        int choice = myScanner.nextInt();
        if(choice == 1 && armor.getLevel() ==1) {
            System.out.println("Enter level: ");
            int lvl = myScanner.nextInt();
            //calls setItemLevel() from Armor class
            armor.setItemLevel(lvl);
            //calls ScaleBySlot() from Armor class then showcases stats.
            armor.scaleBySlot(armor.getSlotType());
            armorScreen(armor);
        }else if(choice == 2) {
            try {
                mainMenu();
            }
            catch (Exception e) {
                System.out.println(e);
            }
        }

    }

    // Method to create weapon by type by choice.
    public void createWeapon() throws InterruptedException {
        String weaponType ="";
        Scanner myScanner = new Scanner(System.in);
        System.out.println("------------------------------------");
        System.out.println("Create new weapon");
        System.out.println("------------------------------------");
        System.out.println("1. Melee");
        System.out.println("2. Ranged");
        System.out.println("3. Magic");
        System.out.println("4. Choose a different item type");

        int choice = myScanner.nextInt();
        if(choice == 1) {
            weaponType = "melee";
            try {
                newWeapon(weaponType);
            }catch (IOException ex) {
                System.out.println(ex);
            }

        }else if(choice == 2) {
            weaponType = "ranged";
            try {
                newWeapon(weaponType);
            }catch (IOException ex) {
                System.out.println(ex);
            }
        }else if(choice == 3) {
            weaponType = "magic";
            try {
                newWeapon(weaponType);
            }catch (IOException ex) {
                System.out.println(ex);
            }
        }else if(choice == 4) {
            createItem();
        }

    }

    // Takes weapontype to let the method create the type of weapon chosen by the user.
    public void newWeapon(String wepType) throws IOException, InterruptedException {
        Scanner myScanner = new Scanner(System.in);
        System.out.println("------------------------------------");
        System.out.println("Create new " + wepType);
        System.out.println("------------------------------------");
        System.out.println("1. Set weapon name");
        System.out.println("2. Choose another type");

        int choice = myScanner.nextInt();
        if(choice == 1) {
            Weapon wep = null;

            if(wepType.equals("melee")) {
                wep = new MeleeWeapon();
            }else if(wepType.equals("ranged")){
                wep = new RangedWeapon();
            }else if(wepType.equals("magic")){
                wep = new MagicWeapon();
            }
            String name ="";
            System.out.println("Enter name: ");

            BufferedReader reader = new BufferedReader(new InputStreamReader(System.in));
            name = reader.readLine();

            //sets weapon name and adds to weapon collection. Sends you yo weaponscreen to see stats and set lvl.
            wep.setName(name);
            weapons.add(wep);
            weaponScreen(wep);

        } else if(choice == 2) {
            createWeapon();
        }

    }

    //Shows the stats of the weapon.
    public void weaponScreen(Weapon weapon) throws InterruptedException {
        Scanner myScanner = new Scanner(System.in);
        System.out.println("------------------------------------");
        System.out.println(weapon.getName() + " - " + weapon.getWeaponType());
        System.out.println("------------------------------------");
        System.out.println("Level :" + weapon.getLevel());
        System.out.println("Damage: " + weapon.getBaseDamage());
        System.out.println("------------------------------------");

        //lets you set an item level. Can only be done once.
        if(weapon.getLevel() == 1) {
            System.out.println("1. Give LVL");
        }

        System.out.println("2. Back to Menu");
        int choice = myScanner.nextInt();
        if(choice == 1 && weapon.getLevel() == 1) {
            System.out.println("Enter level: ");
            int lvl = myScanner.nextInt();
            //setItemLevel from weaponclass wihc scales the item with the new level and calls weaponScreen recursivly to display the new stats.
            weapon.setItemLevel(lvl);
            weaponScreen(weapon);
        }else if(choice == 2) {
            mainMenu();
        }

    }

    // Lets you create new Champions.
    public void createCharacter() throws InterruptedException {
        String heroType = "";
        Scanner myScanner = new Scanner(System.in);
        System.out.println("------------------------------------");
        System.out.println("Create new character");
        System.out.println("------------------------------------");
        System.out.println("1. Mage");
        System.out.println("2. Warrior");
        System.out.println("3. Ranger");
        System.out.println("4. Go back to menu");
        int choice = myScanner.nextInt();
        //Calls newChampion method with herotype depending on choice.
        if(choice == 1) {
            heroType = "mage";
            newChampion(heroType);
        }else if(choice == 2) {
            heroType = "warrior";
            newChampion(heroType);
        }else if(choice == 3) {
            heroType = "ranger";
            newChampion(heroType);
        }else if(choice == 4) {
            try {
                mainMenu();
            }
            catch (InputMismatchException | InterruptedException e) {
                System.out.println(e);
            }
        }

    }

    //takes championType to create a new Champion.
    public void newChampion(String championType) throws InterruptedException {
        Scanner myScanner = new Scanner(System.in);
        System.out.println("------------------------------------");
        System.out.println("Create new " + championType);
        System.out.println("------------------------------------");
        System.out.println("1. Set character name");
        System.out.println("2. Choose another class");
        int choice = myScanner.nextInt();
        //Instansiates a new Champion, which type of champion depending on championtype from createChampion.
        if(choice == 1) {
            Champion hero = null;
            if(championType.equals("mage")) {
                hero = new Mage();
            }else if(championType.equals("warrior")){
                hero = new Warrior();
            }else if(championType.equals("ranger")){
                hero = new Ranger();
            }
            System.out.println("Enter name: ");
            String name = myScanner.next();
            //sets the name of the champion and adds it to the collection of champions.
            hero.setName(name);
            champions.add(hero);
            characterScreen(hero);

        } else if(choice == 2) {
               createCharacter();
        }
    }

    //takes a champion object to display the stats of it.
    public void characterScreen(Champion champion) throws InterruptedException {
        Weapon wep = null;
        //this checks if champion is currently holding a weapon to prevent calling to the hashmap with a key that doesnt exist.
        if(champion.getWeaponEquipment().containsKey("weapon")) {
           wep = champion.getWeaponEquipment().get("weapon");
        }
        //shows various stats.
        Scanner myScanner = new Scanner(System.in);
        System.out.println("------------------------------------");
        System.out.println(champion.getName() + " - " + champion.getHeroType());
        System.out.println("------------------------------------");
        System.out.println("HP: " + (champion.getHp() + caluclateHp(champion)));
        System.out.println("Strength: " + (champion.getStrength()+calculateStrength(champion)));
        System.out.println("Dexterity: " + (champion.getDexterity() + calculateDex(champion)));
        System.out.println("Intelligence: " + (champion.getIntelligence() + calculateInt(champion)));
        System.out.println("Level: " + champion.getLevel());
        System.out.println("XP to next level: " + champion.experienceNeededToLevelUp());
        System.out.println("");
        System.out.println("Attacking for : " + calculateDamage(champion,wep));
        System.out.println("------------------------------------");
        System.out.println("1. Give XP");
        System.out.println("2. Manage Items");
        System.out.println("3. Attack! ");
        System.out.println("4. Back to Menu");
        int choice = myScanner.nextInt();
        //Lets the user give the champion XP to increase level.
        if(choice == 1) {
            System.out.println("Enter ammount of XP: ");
            int xpGained = myScanner.nextInt();
            //gives the champion experience.
            champion.gainExperience2(xpGained);
            //calls the characterScreen method recursively with updatet stats according to xp gained.
            characterScreen(champion);

        }else if(choice ==2) {
            //calls manageItems() to manage the items the champion equips.
            manageItems(champion);

        }else if(choice == 3) {
            //displays a message with the damage the champion is doing if it has a weapon equipped.
            if(champion.getWeaponEquipment().containsKey("weapon")) {
                wep = champion.getWeaponEquipment().get("weapon");
                System.out.println(" ");
                System.out.println(champion.getName() + " deals " +calculateDamage(champion,wep) + " to an annoying murloc");
                System.out.println(" ");
                characterScreen(champion);
            } else {
                System.out.println(" ");
                System.out.println("Your character doesnt equip a weapon so no damage dealt");
                System.out.println(" ");
                characterScreen(champion);
            }

        }
        else if(choice == 4) {
            mainMenu();
        }else{
            throw new IllegalArgumentException("Illegal input");
        }

    }

    //method to manage items equipped of a champion.
    public void manageItems(Champion champion) throws InterruptedException {

        String headName = "";
        String bodyName ="";
        String legName = "";
        String weaponName = "";

        if(champion.getEquipment().containsKey("head")){
            headName = champion.getEquipment().get("head").getName();
        }
        if(champion.getEquipment().containsKey("body")){
            bodyName = champion.getEquipment().get("body").getName();
        }
        if(champion.getEquipment().containsKey("legs")){
            legName= champion.getEquipment().get("legs").getName();
        }
        if(champion.getWeaponEquipment().containsKey("weapon")) {
            weaponName = champion.getWeaponEquipment().get("weapon").getName();
        }

        String slotType = "";
        Scanner myScanner = new Scanner(System.in);
        System.out.println("------------------------------------");
        System.out.println(champion.getName() + "'s Gear");
        System.out.println("------------------------------------");
        System.out.println("Choose a slot to change gear. ");
        System.out.println("");
        System.out.println("1. Head: " + headName);
        System.out.println("2. Body: " + bodyName);
        System.out.println("3. Legs: " + legName);
        System.out.println("4. Weapon: " + weaponName);
        System.out.println(" ");
        System.out.println("5. Back to character Screen. ");

        int choice = myScanner.nextInt();
        // swapItem is called with slotType depending on choice.
        if(choice == 1) {
            slotType = "head";
            swapItem(slotType,champion);
        }else if(choice ==2) {
            slotType = "body";
            swapItem(slotType,champion);
        }else if(choice == 3) {
            slotType = "legs";
            swapItem(slotType,champion);
        }else if(choice == 4) {
            swapWeapon(champion);
        }else if(choice == 5) {
            characterScreen(champion);
        }

    }

    //method to swap between items depending on slot.
    public void swapItem(String slotType, Champion champion) throws InterruptedException {
        Scanner myScanner = new Scanner(System.in);
        int counter = 1;
        int levelCheck = 0;

        //calls filterBySlot which retrieves items that only match slotype from entire collection of created armorpieces.
        Collection<Armor> col = filterBySlot(slotType);

        if(col.isEmpty()) {
            System.out.println("..No items found");
            System.out.println("Go create some!");
            System.out.println("..sending you back to Gear manager...");
            manageItems(champion);
        }

        else{
            System.out.println("------------------------------------");
            System.out.println("Select item you want to equip");
            System.out.println("------------------------------------");

            //iterates over the items found with matching slottype.
            for (Armor arm : col) {
                System.out.println(counter + ". " + arm.getName() + " | Level: " +arm.getLevel());
                counter++;
            }

            System.out.println("");
            System.out.println(counter + ". Remove current piece. ");
            System.out.println(counter +1  + ". Back to Gear Manager. ");
            System.out.println(" ");

            int choice = myScanner.nextInt();
            choice = choice - 1;
            //if back to menu is chosen.
            if(choice + 1 > counter) {
                manageItems(champion);
            }
            //Removes the current piece equipped by a champion and leaves it blank.
            else if(choice+1 == counter) {
                champion.getEquipment().remove(slotType);
                manageItems(champion);
            } else {
            //replaces or sets a new item to a slot if the itemlevel matches with the level of the champion.
            Armor armor = (Armor) col.toArray()[choice];

            if(champion.getLevel() >= armor.getLevel()) {
                champion.getEquipment().put(slotType, (Armor) col.toArray()[choice]);
            }else{
                System.out.println("Itemlevel too high for your character.");
                System.out.println("... Sending you back to Item Manager.....");
            }
            manageItems(champion);
            }
        }
    }

    //this method does exactly the same as swapItem.
    public void swapWeapon(Champion champion) throws InterruptedException {

        Scanner myScanner = new Scanner(System.in);
        int counter = 1;

        if (weapons.isEmpty()) {
            System.out.println("..No weapons found");
            System.out.println("Go create some!");
            System.out.println("..sending you back to Gear manager...");
            manageItems(champion);
        } else {
            System.out.println("------------------------------------");
            System.out.println("Select item you want to equip");
            System.out.println("------------------------------------");

            for (Weapon wep : weapons) {
                System.out.println(counter + ". " + wep.getName() + " | Level" + wep.getLevel());
                counter++;
            }

            System.out.println("");
            System.out.println(counter + ". Remove equipped weapon. ");
            System.out.println(counter +1  + ". Back to Gear Manager. ");

            int choice = myScanner.nextInt();
            choice = choice - 1;

            if(choice + 1 > counter) {
                manageItems(champion);
            } else if(choice+1 == counter) {
                champion.getWeaponEquipment().remove("weapon");
                manageItems(champion);
            }else {

                Weapon wep = (Weapon) weapons.toArray()[choice];
                if (champion.getLevel() >= wep.getLevel()) {
                    champion.getWeaponEquipment().put("weapon", (Weapon) weapons.toArray()[choice]);
                } else {
                    System.out.println("Itemlevel too high for your character.");
                    System.out.println("... Sending you back to Item Manager.....");
                }
                manageItems(champion);
            }
        }
    }

    //method to browse through created items.
    public void browseItem() throws InterruptedException {
        Scanner myScanner = new Scanner(System.in);
        int counter = 0;
        int counter2 = 0;

        if(!weapons.isEmpty()) {
            System.out.println("Weapons");
            System.out.println("------------------------------------");
        }
        for(Weapon wep: weapons) {
            counter ++;
            System.out.println(counter+ ". " + wep.getName() +" | Level: " + wep.getLevel()  + " | " + wep.getWeaponType() + " | Dmg: "+ wep.getBaseDamage());
        }

        if(!armorPieces.isEmpty()) {
            System.out.println(" ");
            System.out.println("Armor pieces");
            System.out.println("------------------------------------");
        }
        for(Armor armor: armorPieces) {
            counter2 ++;
            System.out.println(counter2+ ". " + armor.getName() +" | Level: " + armor.getLevel()  + " | " + armor.getArmorType()+ " " + armor.getSlotType() +
            " | Stats: Strength:  " + armor.getStrength() + ", Dex: " + armor.getDex() + ", Int: " + armor.getIntelligence() + ", HP: " + armor.getHp());
        }
        if(armorPieces.isEmpty() && weapons.isEmpty()) {
            System.out.println("You have no items yet to browse, go create some :)");
        }
        System.out.println(" ");
        System.out.println("Enter 0 to go back to the menu");
        String name = myScanner.next();

        if(name.equals("0")) {
            mainMenu();
        }else{
            throw new IllegalArgumentException("Illegal input");
        }
    }

    //method to filter armor in the collection by slottype.
    public Collection<Armor> filterBySlot(String slotType){
        Collection<Armor> result = new ArrayList<>();
        for(Armor armor: armorPieces) {
            if(armor.getSlotType().equals(slotType)) {
                result.add(armor);
            }
        }
        return result;
    }

    //Browse created characters.
    public void browseCharacters() throws InterruptedException {
        Scanner myScanner = new Scanner(System.in);
        int counter = 0;
        for(Champion champ: champions) {
            counter ++;
            System.out.println(counter+ ". " + champ.getName() +" | Level: " + champ.getLevel()  + " | " + champ.getHeroType());
        }
        System.out.println("------------------------------------");
        System.out.println("Enter name of character to view closer.");
        System.out.println("");
        System.out.println("Enter 0 to go back to the menu");
        String name = myScanner.next();

        if(name.equals("0")) {
            mainMenu();
        }
        for(Champion champ : champions) {
            if(champ.getName().equalsIgnoreCase(name)){
                characterScreen(champ);
            }
        }
        System.out.println("");
        System.out.println("No character with than name, try again.");
        System.out.println("");
        browseCharacters();
    }


    //calculating equpped bonus HP.
    public int caluclateHp(Champion champion) {
        int equipedHp = 0;
        if(champion.getEquipment().containsKey("head")) {
            equipedHp += champion.getEquipment().get("head").getHp();
        }
        if(champion.getEquipment().containsKey("legs")) {
            equipedHp += champion.getEquipment().get("legs").getHp();
        }
        if(champion.getEquipment().containsKey("body")) {
            equipedHp += champion.getEquipment().get("body").getHp();
        }
        return equipedHp;
    }

    //calculating equipped bonus Strength.
    public int calculateStrength(Champion champion) {
        int equpiedStrenght = 0;
        if(champion.getEquipment().containsKey("head")) {
            equpiedStrenght += champion.getEquipment().get("head").getStrength();
        }
        if(champion.getEquipment().containsKey("legs")) {
            equpiedStrenght += champion.getEquipment().get("legs").getStrength();
        }
        if(champion.getEquipment().containsKey("body")) {
            equpiedStrenght += champion.getEquipment().get("body").getStrength();
        }
        return equpiedStrenght;
    }

    // Calculating equipped bonus int
    public int calculateInt(Champion champion) {
        int equippedInt = 0;
        if(champion.getEquipment().containsKey("head")) {
            equippedInt += champion.getEquipment().get("head").getIntelligence();
        }
        if(champion.getEquipment().containsKey("legs")) {
            equippedInt += champion.getEquipment().get("legs").getIntelligence();
        }
        if(champion.getEquipment().containsKey("body")) {
            equippedInt += champion.getEquipment().get("body").getIntelligence();
        }
        return equippedInt;
    }

    // Calcualting equipped bonus Dexterity.
    public int calculateDex(Champion champion) {
        int equippedDex = 0;
        if(champion.getEquipment().containsKey("head")) {
            equippedDex += champion.getEquipment().get("head").getDex();
        }
        if(champion.getEquipment().containsKey("legs")) {
            equippedDex += champion.getEquipment().get("legs").getDex();
        }
        if(champion.getEquipment().containsKey("body")) {
            equippedDex += champion.getEquipment().get("body").getDex();
        }
        return equippedDex;
    }

    // Calculates damage based on BASE STATS, ECUIPPED BONUS STATS AND WEAPON DAMAGE based on weapon type.
    public int calculateDamage(Champion champion, Weapon weapon) {
        int effectiveStats = 0;
        int result = 0;

        if(champion.getWeaponEquipment().containsKey("weapon")){
            if(weapon.getWeaponType().equals("magic")){
                effectiveStats = calculateInt(champion) + champion.getIntelligence() + weapon.getBaseDamage();
                result = effectiveStats * 3;
            }else if(weapon.getWeaponType().equals("melee")){
                effectiveStats = calculateStrength(champion) + champion.getStrength() + weapon.getBaseDamage();
                result = (int) (1.5 * effectiveStats);
            }else if(weapon.getWeaponType().equals("ranged")) {
                effectiveStats = calculateDex(champion) + champion.getDexterity() + weapon.getBaseDamage();
                result = 2 * effectiveStats;
            }
        }
        return result;
    }


    public static void main(String[] args) throws InterruptedException {

        Main main = new Main();
        // ---------------------------------------------//
        // Hardcoded gear and characters to fill from start.
        Weapon wep = new MagicWeapon();
        wep.setName("Atiesh, Greatstaff of the Guardian");
        wep.setItemLevel(2);
        main.weapons.add(wep);

        Weapon jordan = new MagicWeapon();
        jordan.setName("Staff of Jordan");
        jordan.setItemLevel(1);
        main.weapons.add(jordan);

        Weapon mel = new MeleeWeapon();
        mel.setName("Hand of Ragnaros");
        mel.setItemLevel(5);
        main.weapons.add(mel);

        Weapon rang = new RangedWeapon();
        rang.setName("Rhok'delar");
        rang.setItemLevel(10);
        main.weapons.add(rang);


        Armor head = new ClothArmor();
        head.setName("Mish'undare, Circlet of the Mind Flayer");
        head.setSlotType("head");
        head.setItemLevel(5);
        head.scaleBySlot("head");
        main.armorPieces.add(head);

        Armor head2 = new ClothArmor();
        head2.setName("Netherwind Crown");
        head2.setSlotType("head");
        head2.setItemLevel(5);
        head2.scaleBySlot("head");
        main.armorPieces.add(head2);

        Armor head3 = new ClothArmor();
        head3.setName("Nemesis Crown");
        head3.setSlotType("head");
        head3.setItemLevel(1);
        head3.scaleBySlot("head");
        main.armorPieces.add(head3);

        Armor legs = new ClothArmor();
        legs.setName("Nemesis Leggings");
        legs.setSlotType("legs");
        legs.setItemLevel(10);
        legs.scaleBySlot("legs");
        main.armorPieces.add(legs);

        Armor body = new LeatherArmor();
        body.setName("Bloodfang Chestpiece");
        body.setSlotType("body");
        body.setItemLevel(5);
        body.scaleBySlot("body");
        main.armorPieces.add(body);

        Armor plateLegs = new PlateArmor();
        plateLegs.setName("Judgement Breastplate");
        plateLegs.setSlotType("body");
        plateLegs.setItemLevel(15);
        plateLegs.scaleBySlot("body");
        main.armorPieces.add(plateLegs);

        Champion mage = new Mage();
        mage.setName("Xaryu");
        mage.gainExperience(150);

        // ---------------------------------------------//

        main.champions.add(mage);

        main.mainMenu();

    }
}
