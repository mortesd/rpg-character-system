package com.company.items;

public abstract class Armor implements Item{

    protected int level;
    protected String slotType;
    protected String name;

    protected int hp;
    protected int dex;
    protected int strength;
    protected int intelligence;

    protected String armorType;

    public Armor(int level, int hp, int dex, int strength, int intelligence, String armorType) {
        this.level = level;
        this.hp = hp;
        this.dex = dex;
        this.strength = strength;
        this.intelligence = intelligence;
        this.armorType = armorType;
    }

    //Scales down the stats of the pieces according to slottype.
    public void scaleBySlot(String slotType) {

        if(slotType.equals("legs")) {

            int hpScaled = (int)(this.getHp()*(60.0/100.0f));
            this.setHp(hpScaled);

            int dexScaled = (int)(this.getDex()*(60.0/100.0f));
            this.setDex(dexScaled);

            int strScaled = (int)(this.getStrength()*(60.0/100.0f));
            this.setStrength(strScaled);

            int intScaled = (int)(this.getIntelligence()*(60.0/100.0f));
            this.setIntelligence(intScaled);

        }else if(slotType.equals("head")){

            int hpScaled = (int)(this.getHp()*(80.0/100.0f));
            this.setHp(hpScaled);

            int dexScaled = (int)(this.getDex()*(80.0/100.0f));
            this.setDex(dexScaled);

            int strScaled = (int)(this.getStrength()*(80.0/100.0f));
            this.setStrength(strScaled);

            int intScaled = (int)(this.getIntelligence()*(80.0/100.0f));
            this.setIntelligence(intScaled);

        }else if(slotType.equals("body")){

        }

    }


    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getSlotType() {
        return slotType;
    }

    public void setSlotType(String slotType) {
        this.slotType = slotType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getHp() {
        return hp;
    }

    public void setHp(int hp) {
        this.hp = hp;
    }

    public int getDex() {
        return dex;
    }

    public void setDex(int dex) {
        this.dex = dex;
    }

    public int getStrength() {
        return strength;
    }

    public void setStrength(int strength) {
        this.strength = strength;
    }

    public int getIntelligence() {
        return intelligence;
    }

    public void setIntelligence(int intelligence) {
        this.intelligence = intelligence;
    }

    public String getArmorType() {
        return armorType;
    }

    public void setArmorType(String armorType) {
        this.armorType = armorType;
    }
}
