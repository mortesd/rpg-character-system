package com.company.items;

import com.company.Heroes.Champion;

public abstract class Weapon implements Item{

    protected int baseDamage;
    protected int level;
    protected String weaponType;
    protected String name;

    public Weapon(int baseDamage, int level, String weaponType){
        super();
        this.baseDamage = baseDamage;
        this.level = level;
        this.weaponType = weaponType;
    }

    public int getBaseDamage() {
        return baseDamage;
    }

    public void setBaseDamage(int baseDamage) {
        this.baseDamage = baseDamage;
    }

    public int getLevel() {
        return level;
    }

    public void setLevel(int level) {
        this.level = level;
    }

    public String getWeaponType() {
        return weaponType;
    }

    public void setWeaponType(String weaponType) {
        this.weaponType = weaponType;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
