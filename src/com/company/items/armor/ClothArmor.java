package com.company.items.armor;

import com.company.items.Armor;

public class ClothArmor extends Armor {


    public ClothArmor(){
        super(1,10, 1, 0, 3, "cloth");
    }

    //sets the itemlevel to param.
    @Override
    public void setItemLevel(int lvl) {

        for (int i = 1; i < lvl; i++) {
            level += 1;
            hp += 5;
            intelligence += 2;
            dex += 1;
        }
    }
}
