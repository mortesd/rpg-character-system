package com.company.items.armor;

import com.company.items.Armor;

public class LeatherArmor extends Armor {


    public LeatherArmor(){
        super(1,20, 3, 1, 0, "leather");
    }

    //sets the itemlevel to param.
    @Override
    public void setItemLevel(int lvl) {

        for (int i = 1; i < lvl; i++) {
         /*   this.setLevel(this.getLevel() + 1);
            this.setHp(this.getHp() + 8);
            this.setStrength(this.getStrength() + 1);
            this.setDex(this.getDex() + 2);*/

            level += 1;
            hp += 8;
            strength += 1;
            dex += 2;
        }

    }
}
