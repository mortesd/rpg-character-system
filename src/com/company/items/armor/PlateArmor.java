package com.company.items.armor;

import com.company.items.Armor;

public class PlateArmor extends Armor {


    public PlateArmor(){
        super(1,30, 1, 3, 0, "plate");
    }

    //sets the itemlevel to param.
    @Override
    public void setItemLevel(int lvl) {

        for (int i = 1; i < lvl; i++) {
            level += 1;
            hp += 12;
            strength += 2;
            dex += 1;
        }

    }
}
