package com.company.items.weapon;

import com.company.items.Weapon;

public class MagicWeapon extends Weapon {

    public MagicWeapon() {
        super(25,1,"magic");
    }

    @Override
    public void setItemLevel(int lvl) {
        for (int i = 1; i < lvl; i++) {
            level = level + 1;
            baseDamage = baseDamage +2;
        }
    }

}
