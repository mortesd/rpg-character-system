package com.company.items.weapon;

import com.company.items.Armor;
import com.company.items.Weapon;

public class MeleeWeapon extends Weapon {

    public MeleeWeapon() {
        super(15,1,"melee");
    }


    @Override
    public void setItemLevel(int lvl) {
        for (int i = 1; i < lvl; i++) {
            level = level + 1;
            baseDamage = baseDamage +2;
        }
    }
}
