package com.company.items.weapon;

import com.company.items.Armor;
import com.company.items.Weapon;

public class RangedWeapon extends Weapon {

    public RangedWeapon() {
        super(5,1,"ranged");
    }

    @Override
    public void setItemLevel(int lvl) {
        for (int i = 1; i < lvl; i++) {
            level = level + 1;
            baseDamage = baseDamage + 3;
        }
    }

}
